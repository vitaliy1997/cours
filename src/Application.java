import library.view.DetectiveView;
import library.view.MainView;
import library.view.Print;

public class Application {
    public static void main(String[] args) {

        new MainView().show();
    }
}
