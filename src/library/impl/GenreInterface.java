package library.impl;

public interface GenreInterface {

    String getClassTitle();

    int getClassYear();

    String getClassGenre();

}
