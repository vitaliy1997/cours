package library.library;

import library.en.HistoryEnum;
import library.enumAdv.HistoryAdv;
import library.impl.BasicInterface;
import library.impl.GenreInterface;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class History implements GenreInterface  {

    @Override
    public String getClassTitle() {
        return Arrays.toString(HistoryEnum.values());
    }

    @Override
    public int getClassYear() {
        return 0;
    }

    @Override
    public String getClassGenre() {
        return Arrays.toString(HistoryAdv.values());
    }
    public void readUkraine(){
        try(FileReader reader = new FileReader("Ukraine.txt"))
        {

            int c;
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public void readWorld(){
        try(FileReader reader = new FileReader("World.txt"))
        {

            int c;
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}
