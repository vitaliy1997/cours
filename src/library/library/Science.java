package library.library;

import library.en.ScienceEnum;
import library.impl.GenreInterface;

import java.util.Arrays;

public class Science implements GenreInterface {
    @Override
    public String getClassTitle() {
        return Arrays.toString(ScienceEnum.values());
    }

    @Override
    public int getClassYear() {
        return 0;
    }

    @Override
    public String getClassGenre() {
        return null;
    }
}
