package library.library;

import library.en.RomanEnum;
import library.impl.GenreInterface;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Roman implements GenreInterface {
    @Override
    public String getClassTitle() {
        return Arrays.toString(RomanEnum.values());
    }

    @Override
    public int getClassYear() {
        return 0;
    }

    @Override
    public String getClassGenre() {
        return null;
    }
}
