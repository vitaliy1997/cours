package library.library;

import library.en.DetectiveEnum;
import library.enumAdv.DetectiveAdv;
import library.impl.GenreInterface;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class Detective implements GenreInterface {


    DetectiveEnum anEnum;

    public String sherlock() {

        return Arrays.toString(DetectiveEnum.values());
    }

    @Override
    public String getClassTitle() {

        return Arrays.toString(DetectiveEnum.values());
    }

    @Override
    public int getClassYear() {
        for (DetectiveEnum s : DetectiveEnum.values()) {
            return s.getDate();
        }
        return 0;
    }

    @Override
    public String getClassGenre() {
        return Arrays.toString(DetectiveAdv.values());
    }

    public void readSherlock(){
        try(FileReader reader = new FileReader("Sherlock.txt"))
        {

            int c;
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public void readPuaro(){
        try(FileReader reader = new FileReader("Puaro.txt"))
        {

            int c;
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

}
