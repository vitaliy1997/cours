package library.library;

import library.en.FantasyEnum;
import library.enumAdv.DetectiveAdv;
import library.enumAdv.FantasyAdv;
import library.impl.GenreInterface;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Fantasy implements GenreInterface {
    @Override
    public String getClassTitle() {

        return Arrays.toString(FantasyEnum.values());
    }

    @Override
    public int getClassYear() {
        return 0;
    }

    @Override
    public String getClassGenre() {
        return Arrays.toString(FantasyAdv.values());
    }

    public void readHarry(){
        try(FileReader reader = new FileReader("HarryPotter.txt"))
        {

            int c;
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public void readTower(){
        try(FileReader reader = new FileReader("DarkTower.txt"))
        {

            int c;
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}
