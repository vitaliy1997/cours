package library.view;

import library.en.DetectiveEnum;
import library.library.Detective;

import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class DetectiveView {

    Detective detective = new Detective();
    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Print> putMenu = new LinkedHashMap<>();
    Scanner sc = new Scanner(System.in);

    public  DetectiveView() {

        menu.put("1","1-get Advanced");
        menu.put("2","2-get Sherlock");
        menu.put("3","3-get Puaro");
        menu.put("B","B-Back");

        putMenu.put("1",this::pressButton1);
        putMenu.put("2",this::pressButton2);
        putMenu.put("3",this::pressButton3);


    }

    private void pressButton3() {
        detective.readPuaro();
    }

    private void pressButton1() {

        System.out.println(detective.getClassGenre());
    }


    private void pressButton2() {
        detective.readSherlock();
    }

    //-------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

}
