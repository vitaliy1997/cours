package library.view;

import library.library.Detective;
import library.library.Fantasy;
import library.library.History;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {


    HistoryView hisView;
    History history;
    FantasyView fanView;
    Fantasy fantasy;
    DetectiveView detView;
    Detective detective;
    Map<String, Detective> menu1 = new LinkedHashMap<>();
    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Print> putMenu = new LinkedHashMap<>();
    Scanner scanner = new Scanner(System.in);

    public MainView() {
        menu.put("1", "1-Detective");
        menu.put("2", "2-History");
        menu.put("3", "3-Horror");
        menu.put("4", "4-Roman");
        menu.put("5", "5-Science");
        menu.put("6", "6-Fantasy");
        menu.put("E", "E-Exit");


        putMenu.put("1", this::pressButton1);
        putMenu.put("2", this::pressButton2);
        putMenu.put("3", this::pressButton3);
        putMenu.put("4", this::pressButton4);
        putMenu.put("5", this::pressButton5);
        putMenu.put("6", this::pressButton6);
    }

    private void pressButton6() {
        fantasy = new Fantasy();
        fanView = new FantasyView();

        System.out.println(fantasy.getClassTitle());
        fanView.show();

    }

    private void pressButton5() {
    }

    private void pressButton4() {
    }

    private void pressButton3() {
    }

    private void pressButton2() {
        history=new History();
        history.getClassTitle();
        hisView=new HistoryView();
        hisView.show();
    }

    private void pressButton1() {
        detective = new Detective();
        detView = new DetectiveView();
        System.out.println(detective.getClassTitle());
        detView.show();

    }

    //----------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }

}
