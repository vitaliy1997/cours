package library.view;

@FunctionalInterface
public interface Print {

    void print();
}
