package library.view;

import library.library.History;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class HistoryView {

    History history = new History();
    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Print> putMenu = new LinkedHashMap<>();
    Scanner sc = new Scanner(System.in);

    public HistoryView(){

        menu.put("1","1-Adv");
        menu.put("2","2-get text About history of Ukraine");
        menu.put("3","3-get Text about History of the world");
        menu.put("B","B-Back");

        putMenu.put("1",this::pressButton1);
        putMenu.put("2",this::pressButton2);
        putMenu.put("3",this::pressButton3);

    }

    private void pressButton1() {
        history.getClassGenre();
    }

    private void pressButton2() {

        history.readUkraine();
    }

    private void pressButton3() {
        history.readWorld();
    }


    //-----------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

}
