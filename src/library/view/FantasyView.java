package library.view;

import library.library.Fantasy;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class FantasyView {

    Fantasy fantasy;
    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Print> putMenu = new LinkedHashMap<>();
    Scanner sc = new Scanner(System.in);
    
    public FantasyView(){
        
        menu.put("1","1-Adv");
        menu.put("2","2-get Text about Garry Potter");
        menu.put("3","3-get Text Dark Tower");
        menu.put("B","B-Back");
        
        putMenu.put("1",this::pressButton1);
        putMenu.put("2",this::pressButton2);
        putMenu.put("3",this::pressButton3);

    }

    private void pressButton3() {
        fantasy=new Fantasy();
        fantasy.readTower();
    }

    private void pressButton1() {
        fantasy=new Fantasy();
        System.out.println(fantasy.getClassGenre());

    }

    private void pressButton2() {
        fantasy=new Fantasy();
        fantasy.readHarry();

    }

    //-----------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                putMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("B"));
    }

}
