package library.enumAdv;

import library.library.Science;

public enum FantasyAdv {

    GarryPoter("Bla-Bla-Bla","Rouling"),
    BlackTower("Bla-Blu-Bla","King");

    private String description;
    private String writer;

    FantasyAdv(String description,String writer){

        this.description=description;
        this.writer=writer;
    }

    public String getDescription() {
        return description;
    }

    public String getWriter() {
        return writer;
    }

    @Override
    public String toString() {
        return "FantasyAdv{" +
                "description='" + description + '\'' +
                ", writer='" + writer + '\'' +
                '}';
    }
}
