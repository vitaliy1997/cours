package library.enumAdv;

public enum HistoryAdv {

    HistoryUkraine("Історія України",1991,6,"Bla-bla","All people in Ukraine"),
    HistoryWorld("Світова Історія",1992,5,"Bla","All people on the world");

    private String title;
    private int date;
    private int numb;
    private String description;
    private String writer;

    HistoryAdv(String title, int date, int numb, String description, String writer) {
        this.title = title;
        this.date = date;
        this.numb = numb;
        this.description = description;
        this.writer = writer;
    }

    public String getTitle() {
        return title;
    }

    public int getDate() {
        return date;
    }

    public int getNumb() {
        return numb;
    }

    public String getDescription() {
        return description;
    }

    public String getWriter() {
        return writer;
    }

    @Override
    public String toString() {
        return "HistoryAdv{" +
                "title='" + title + '\'' +
                ", date=" + date +
                ", numb=" + numb +
                ", description='" + description + '\'' +
                ", writer='" + writer + '\'' +
                '}';
    }
}
