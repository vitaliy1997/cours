package library.enumAdv;

public enum DetectiveAdv {

    SherlockHolmes("Ble-Ble-Ble","Конан Доіл"),
    Puaro("Бла-Бла-Бла","Агата Крісті");

    private String description;
    private String writer;

    DetectiveAdv(String description,String writer){
        this.description=description;
        this.writer=writer;
    }

    public String getDescription() {
        return description;
    }

    public String getWriter() {
        return writer;
    }

    @Override
    public String toString() {
        return "DetectiveAdv{" +
                "description='" + description + '\'' +
                ", writer='" + writer + '\'' +
                '}';
    }
}
