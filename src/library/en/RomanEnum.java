package library.en;

import library.library.Roman;

public enum RomanEnum {

    RomeoAnd("Ромео і Джульєта",1854),
    Roman("Ше якийсь роман",1323);

    private String title;
    private int date;

    RomanEnum(String title, int date){
        this.date=date;
        this.title=title;
    }

    public int getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "RomanEnum{" +
                "title='" + title + '\'' +
                ", date=" + date +
                '}';
    }
}
