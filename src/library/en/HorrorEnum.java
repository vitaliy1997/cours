package library.en;

public enum HorrorEnum {

    It("Воно",1978),
    BookKing("Ще якісь книги Кінга",1947);

    private String title;
    private int date;

    HorrorEnum(String title, int date){
        this.date=date;
        this.title=title;
    }

    public int getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return " " +
                "назва='" + title + '\'' +
                ", дата=" + date;
    }


    public String title() {
        return "HorrorEnum{" +
                "title='" + title + '\'' +
                '}';
    }
}
