package library.en;

public enum FantasyEnum {

    GarryPotter("Гарі Потер",1997),
    BlackTower("Чорна вежа",1996);

        private String title;
    private int date;

    FantasyEnum(String title, int date){
        this.date=date;
        this.title=title;
    }

    public int getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "FantasyEnum{" +
                "title='" + title + '\'' +
                ", date=" + date +
                '}';
    }
}
