package library.en;

public enum  ScienceEnum {

    Galaxy("Галактика",-234),
    Physics("Фізика",-100);

    private String title;
    private int date;

    ScienceEnum(String title, int date){
        this.date=date;
        this.title=title;
    }

    public int getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "ScienceEnum{" +
                "title='" + title + '\'' +
                ", date=" + date +
                '}';
    }
}
