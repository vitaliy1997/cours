package library.en;

import library.impl.BasicInterface;
import library.impl.GenreInterface;

import java.util.List;

public enum DetectiveEnum {


    SherlockHolmes("Шерлок Холмс", 1234),
    Puaro("Еркюль Пуаро", 4321);

    private String title;
    private int date;

    DetectiveEnum(String title, int date) {
        this.date = date;
        this.title = title;
    }

    public int getDate() {
        return date;
    }

    public String getTitle() {
        this.title = title;
        return title;
    }

    @Override
    public String toString() {
        return " " +
                "title='" + title + '\'' +
                ", date=" + date +
                '}';
    }
}