package library.en;


public enum HistoryEnum {

    HistoryUkraine("Історія України",1991),
    HistoryWorld("Світова Історія",1992);

    private String title;
    private int date;

    HistoryEnum(String title, int date){
        this.date=date;
        this.title=title;
    }

    public int getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "HistoryEnum{" +
                "title='" + title + '\'' +
                ", date=" + date +
                '}';
    }
}
